ics-ans-role-artifactory
===================

Ansible role to install artifactory.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
artifactory_network: artifactory

artifactory_image_tag: 7.4.3
artifactory_container_name: artifactory
artifactory_dir: /var/opt/jfrog/artifactory
artifactory_uid: 1030
artifactory_gid: 1030
artifactory_plugins: []
artifactory_frontend_rule: "Host:{{ ansible_fqdn }}"

artifactory_database_image: "postgres:{{ artifactory_database_image_tag }}"
artifactory_database_image_tag: 9.5.2
artifactory_database_container_name: artifactory_database
artifactory_database_username: artifactory
artifactory_database_password: password
artifactory_database_dir: /var/lib/postgresql/data

# Artifactory license will be applied if the following variable is defined
# artifactory_license:
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-artifactory
```

License
-------

BSD 2-clause
